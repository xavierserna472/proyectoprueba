var head = document.getElementById("head");
head.innerHTML = `
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Denuncia Online</title>
<link rel="stylesheet" href="/css/style.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@400;700&display=swap" rel="stylesheet">
`

var header = document.getElementById("header");
header.innerHTML = `
<div class="Header-Box1" >
  <div class="Header-Box2">
    <img class="Header-Logo_PNP" src="/img/pnp-logo.png">
      <a class="Header-Enlace1" href="./index.html">
        <i class="fa-solid fa-unlock-keyhole"></i>
        Finalizar Proceso
      </a>
  </div>
</div >
`

var footer = document.getElementById("footer");
footer.innerHTML = `
<div class="Footer-Box1">
<div class="Footer-Box1A">
  <a href="">TERMINOS Y CONDICIONES</a>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="">DOCUMENTACIÓN</a>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="">NORMAS Y DISPOSICIONES</a>
  <p>
    Dirección de Tecnologías de la Información y Comunicaciones PNP
  </p>
</div>
<div class="Footer-Box1B">
  <img class="Footer-Logo_PNP" src="/img/pnp-logo.png">
  <p>
    Departamento de Gestión de Proyectos y Desarrollo de Sistemas de Información
    <br>
    Mesa de Ayuda y Atención al Ciudadano: 0800-17008
    <br>
    Copyright © 2022 - Todos los derechos reservados
  </p>
</div>
</div>
`


// en ves de js llamarle servicios?