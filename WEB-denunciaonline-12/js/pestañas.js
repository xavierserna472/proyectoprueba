console.log(1)

function abrirPestaña(evt, cityName) {
  var i, contenidoPestaña, pestaña;
  contenidoPestaña = document.getElementsByClassName("contPestañas__contenido");
  for (i = 0; i < contenidoPestaña.length; i++) {
    contenidoPestaña[i].style.display = "none";
  }
  pestaña = document.getElementsByClassName("contPestañas__cabeceras__pestaña");
  for (i = 0; i < pestaña.length; i++) {
    pestaña[i].className = pestaña[i].className.replace(" activo", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " activo";
}

// Va al elemento con id="iniciaAbierto" y le da click
document.getElementById("iniciaAbierto").click();