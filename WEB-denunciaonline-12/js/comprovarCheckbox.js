(function () {
  var formulario = document.getElementsByName('formulario')[0],
    boton = document.getElementById('btn');

  var validarCheckbox = function (e) {
    if (formulario.acepto.checked == false) {
      alert("Acepte los terminos para continuar");
      e.preventDefault();
    } else {
      window.location.href = "./1_validacion_datos.html";
      e.preventDefault();
    }
  };

  var validar = function (e) {
    validarCheckbox(e);
  };

  formulario, addEventListener("submit", validar);
}())