var contDatGen__formB = document.getElementById('contDatGen__formB');

contDatGen__formB.addEventListener('submit', function (e) {
  e.preventDefault()

  var datosB = new FormData(contDatGen__formB);

  var contDatGen__formA = document.getElementById('contDatGen__formA');
  var datosA = new FormData(contDatGen__formA);

  var obj = {
    clienteUsuario: "PRUEBA_USER",
    clientePassword: "123456",
    servicioCodigo: "WS_TOKEN_CORREO_PNP",
    tipoDocUserClieFin: "1",
    nroDocUserClieFin: "22",
    clienteSistema: "fafafaf",
    clienteIp: "192.168.1.1",
    clienteMac: "3D-F2-C9-A6-B3-4F",
    usuario: datosA.get('DNI'),
    token: datosB.get('codigoValidacion')
  }

  console.log(obj)

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify(obj);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("http://172.31.1.168:7013/api_correo_pnp/api/genera/token", requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    // .then(contenido.innerHTML = `<p>Se le ha enviado un código a su correo reviselo e introduscalo en el casillero de abajo, podría tardar hasta 10 minutos</p>`)
    .catch(error => console.log('error', error));

})